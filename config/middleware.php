<?php

return [
	'Api'   => [
		'\app\middleware\Cross',
		'\app\middleware\Check',
		'\app\middleware\Validate',
	],
	'Auth'  => '\app\middleware\Auth',
	'Admin' => '\app\middleware\Admin',
];