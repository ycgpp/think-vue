const files = require.context('./modules', false, /\.js$/)
const modules = []

files.keys().forEach(key => {
	modules.push(files(key).default)
})

/**
 * 在主框架内显示
 */
const frameIn = [
	{
		path: '/',
		redirect: { name: 'index' },
		component: () => import('@/layout/header-aside'),
		children: [
			// 首页 必须 name:index
			{
				path: 'index',
				name: 'index',
				meta: {
					auth: true
				},
				component: () => import('@/pages/index')
			},
			// 刷新页面 必须保留
			{
				path: 'refresh',
				name: 'refresh',
				hidden: true,
				component: {
					beforeRouteEnter (to, from, next) {
						next(vm => vm.$router.replace(from.fullPath))
					},
					render: h => h()
				}
			},
			// 页面重定向 必须保留
			{
				path: 'redirect/:route*',
				name: 'redirect',
				hidden: true,
				component: {
					beforeRouteEnter (to, from, next) {
						next(vm => vm.$router.replace(JSON.parse(from.params.route)))
					},
					render: h => h()
				}
			}
		]
	}
]

/**
 * 在主框架之外显示
 */
const frameOut = [
	// 登录
	{
		path: '/login',
		name: 'login',
		meta: {
			title: '用户登录'
		},
		component: () => import('@/pages/login')
	}
]

/**
 * 错误页面
 */
const errorPage = [
	// 404
	{
		path: '*',
		name: '404',
		component: () => import('@/pages/error-page-404')
	}
]

// 导出需要显示菜单的
export const frameInRoutes = [...frameIn, ...modules];

// 重新组织后导出
export default [
	...frameIn,
	...modules,
	...frameOut,
	...errorPage
]
