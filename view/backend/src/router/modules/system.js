const meta = { requiresAuth: true }

export default {
	path: '/system',
	name: 'system',
	meta,
	redirect: { name: 'system-menu' },
	component: () => import('@/layout/header-aside'),
	children: (pre => [
		{ path: 'menu', name: `${pre}menu`, component: () => import('@/pages/system/menu'), meta: { meta, title: '菜单管理' } },
		{ path: 'user', name: `${pre}user`, component: () => import('@/pages/system/user'), meta: { meta, title: '用户管理' } },
		{ path: 'role', name: `${pre}role`, component: () => import('@/pages/system/role'), meta: { meta, title: '角色管理' } },
		{ path: 'auth', name: `${pre}auth`, component: () => import('@/pages/system/auth'), meta: { meta, title: '权限管理' } },
		{ path: 'department', name: `${pre}department`, component: () => import('@/pages/system/department'), meta: { meta, title: '组织架构' } },
		{ path: 'interface', name: `${pre}interface`, component: () => import('@/pages/system/interface'), meta: { meta, title: '接口管理' } }
	])('system-')
}