// polyfill
import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import http from '@/utils/http';

import '@/assets/style/style.css';

// 菜单和路由设置
import router from './router'

import store from '@/store'

// 核心插件
import Admin from '@/plugin/admin'

// [ 可选插件组件 ]D2-Crud
import D2Crud from '@d2-projects/d2-crud'
// [ 可选插件组件 ] 图表
import VCharts from 'v-charts'
// [ 可选插件组件 ] 右键菜单
import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
// [ 可选插件组件 ] JSON 树状视图
import vueJsonTreeView from 'vue-json-tree-view'
// [ 可选插件组件 ] 网格布局组件
import { GridLayout, GridItem } from 'vue-grid-layout'
// [ 可选插件组件 ] 区域划分组件
import SplitPane from 'vue-splitpane'
// [ 可选插件组件 ] UEditor
import VueUeditorWrap from 'vue-ueditor-wrap'

Vue.use(Admin);
Vue.use(D2Crud);
Vue.use(VCharts)
Vue.use(contentmenu)
Vue.use(vueJsonTreeView)
Vue.component('d2-grid-layout', GridLayout)
Vue.component('d2-grid-item', GridItem)
Vue.component('SplitPane', SplitPane)
Vue.component('VueUeditorWrap', VueUeditorWrap)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App),
	created () {
	},
	mounted () {
		// 展示系统信息
		this.$store.commit('releases/versionShow')
		// 用户登录后从数据库加载一系列的设置
		this.$store.dispatch('account/load')
		// 获取并记录用户 UA
		this.$store.commit('ua/get')
		// 初始化全屏监听
		this.$store.dispatch('fullscreen/listen')
	},
	watch: {
		// 监听路由 控制侧边栏显示
		'$route.matched' (val) {
			let fullAside = this.$store.state.menu.fullAside
			if (fullAside) {
				const _side = fullAside.filter(menu => menu.path === val[0].path)
				this.$store.commit('menu/asideSet', _side.length > 0 ? _side[0].children : [])
			}
		}
	}
}).$mount('#app')