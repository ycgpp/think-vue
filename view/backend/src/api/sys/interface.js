import request from '@/utils/http'
export default {
	getInterface(id) {
		return request({
			url: '/interfaces/detail?id=' + id,
			method: 'get',
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},
	refresh(){
		return request({
			url: '/interfaces/refresh',
			method: 'get',
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},
	getInterfacePagedList(query) {
		return request({
			url: '/interfaces/lists',
			method: 'get',
			params: query
		})
	},
	saveInterface(data) {
		let url = "/interfaces/add";
		if (data.id) {
			url = "/interfaces/edit";
		}
		return request({
			url: url,
			method: 'post',
			data: data,
			success: {
				type: 'message',
				options: {
					message: '保存成功',
					type: 'success'
				}
			}
		})
	},
	delInterface(id) {
		return request({
			url: '/interfaces/del',
			method: 'delete',
			params: {
				id: id
			},
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	},
	delInterfaces(ids) {
		return request({
			url: '/interfaces/batchdel',
			method: 'delete',
			params: ids,
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	},
	relateInterface(data) {
		return request({
			url: '/interfaces/relate',
			method: 'post',
			data: data
		})
	}
}