import request from '@/utils/http'
export default {
	getUser(uid) {
		return request({
			url: '/user/detail?uid=' + uid,
			method: 'get',
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},
	getUserPermissionInfo() {
		return request({
			url: '/user/info',
			method: 'get'
		})
	},
	getUserPagedList(query) {
		return request({
			url: '/user/lists',
			method: 'get',
			params: query
		})
	},
	editRoleUser(data) {
		return request({
			url: '/user/editroleuser',
			method: 'post',
			data: data
		})
	},
	saveUser(data) {
		let url = "/user/add";
		if (data.uid) {
			url = "/user/edit";
		}
		return request({
			url: url,
			method: 'post',
			data: data,
			success: {
				type: 'message',
				options: {
					message: '保存成功',
					type: 'success'
				}
			}
		})
	},
	delUser(id) {
		return request({
			url: '/user/delete',
			method: 'delete',
			params: {
				id: id
			},
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	},
	delUsers(ids) {
		return request({
			url: '/user/delete',
			method: 'delete',
			params: ids,
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	}
}