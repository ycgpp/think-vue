import http from '@/utils/http'

export default{
	getMenuList() {
		return http({
			url: '/menu/lists',
			method: 'get',
			interfaceCheck: true,
			loading: {
				type: 'loading',
				options: {
					fullscreen: true,
					lock: true,
					text: '加载中...',
					spinner: 'el-icon-loading',
					background: 'rgba(0, 0, 0, 0.8)'
				}
			}
		})
	},

	getMenu(id) {
		return http({
			url: '/menu/detail?id='+id,
			method: 'get',
			interfaceCheck: true,
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},

	saveMenu(data) {
		let url = "/menu/add";
		if (data.id) {
			url = "/menu/edit";
		}
		return http({
			url: url,
			method: 'post',
			interfaceCheck: true,
			data,
			loading: {
				type: 'nprogress',
				interval: 1000
			},
			success: {
				type: 'message',
				options: {
					message: '保存成功',
					type: 'success'
				}
			}
		})
	},

	delMenu(id) {
		return http({
			url: '/menu/delete?id=' + id,
			interfaceCheck: true,
			method: 'delete',
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	}
}