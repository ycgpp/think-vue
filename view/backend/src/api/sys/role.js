import request from '@/utils/http'
export default {
	getRole(id) {
		return request({
			url: '/role/detail?id=' + id,
			method: 'get',
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},
	getRolePagedList(query) {
		return request({
			url: '/role/lists',
			method: 'get',
			params: query,
			loading: {
				type: 'nprogress',
				interval: 500
			}
		})
	},
	delRole(id) {
		return request({
			url: '/role/del',
			method: 'delete',
			params: {
				id: id
			},
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	},
	delRoles(ids) {
		return request({
			url: '/role/batchdel',
			method: 'delete',
			params: ids,
			success: {
				type: 'message',
				options: {
					message: '删除成功',
					type: 'success'
				}
			}
		})
	},
	saveRole(data) {
		let url = "/role/add";
		if (data.id) {
			url = "/role/edit";
		}
		return request({
			url: url,
			method: 'post',
			data: data,
			success: {
				type: 'message',
				options: {
					message: '保存成功',
					type: 'success'
				}
			}
		})
	},
	getRolePermissions(roleId) {
		return request({
			url: '/role/getpermissions?role_id=' + roleId,
			method: 'get',
			loading: {
				type: 'loading',
				options: {
					fullscreen: true,
					lock: true,
					text: '加载中...',
					spinner: 'el-icon-loading',
					background: 'rgba(0, 0, 0, 0.8)'
				}
			}
		})
	},
	savePermission(data) {
		return request({
			url: '/role/savepermission',
			method: 'post',
			data: data,
			success: {
				type: 'message',
				options: {
					message: '保存成功',
					type: 'success'
				}
			}
		})
	}
}