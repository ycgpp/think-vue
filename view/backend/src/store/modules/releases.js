import util from '@/utils/util.js'

export default {
  namespaced: true,
  mutations: {
    /**
     * @description 显示版本信息
     * @param {Object} state vuex state
     */
    versionShow () {
      util.log.capsule('SentCMS', `v${process.env.VUE_APP_VERSION}`)
      console.log('Gitee https://gitee.com/ycgpp/think-vue')
      console.log('Doc    https://gitee.com/ycgpp/think-vue/issues')
      console.log('请不要吝啬您的 star，谢谢 ~')
    }
  }
}
