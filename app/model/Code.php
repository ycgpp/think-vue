<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\model;

use think\Model;

/**
 * @title 验证码模型
 */
class Code extends Model {

	protected $pk = 'id';

	public function check($mobile, $code){
		$code = $this->where('mobile', $mobile)->find();
		if ($code['code'] == $code) {
			return true;
		}else{
			$this->error = "验证码错误！";
			return false;
		}
	}
}