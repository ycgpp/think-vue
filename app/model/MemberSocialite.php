<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\common\model;

/**
* 用户模型
*/
class MemberSocialite extends Base{

	protected $type = array(
		'uid'  => 'integer',
	);

	public function register(){
		if ($socialite->where('openid', $data['info']['openid'])->value('id')) {
			$this->error = "请勿重复注册！";
			return false;
		} else {
			$account = array(
				'username' => $data['info']['openid'],
				'password' => rand_string(8),
			);
			$result = self::create($account);
			if (false !== $result) {
				$socialite_info         = $data['info'];
				$user                   = $this->where('uid', $result->uid)->find();
				$socialite_info['uid']  = $user['uid'];
				$socialite_info['type'] = $data['open_type'];
				$socialite->register($socialite_info);
			} else {
				$this->error = "注册失败！";
				return false;
			}
		}
	}

	public function wechatLogin($user_info){
		$socialite = $this->where('openid', $user_info['openid'])->where('type', 'wechat')->find();
		$member = new Member();
		if ($socialite) {
			$this->where('id', $socialite['id'])->update($user_info);
			if ($socialite['uid'] > 0) {
				//绑定了用户则自动登录
				$user = $member->where('uid', $socialite['uid'])->find();
				$member->autoLogin($user);
			}
		}else{
			$user_info['type'] = 'wechat';
			$this->insert($user_info);
		}
	}
}