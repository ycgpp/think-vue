<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\model;

use think\Model;

/**
 * @title: 菜单模型
 */
class Route extends Model {

	protected function getMetaAttr($value, $data){
		return array(
			'title'  => $data['title']
		);
	}

	public function getAccessRoutes($where = array(), $tree = true){
		$res = $this->where($where)->select();
		$list = $res->append(array('meta'))->toArray();
		if ($tree) {
			$list = list_to_tree($list, 'id', 'pid', 'children');
		}
		return $list;
	}
}