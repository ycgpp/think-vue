<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\model;

use think\Model;

/**
 * @title: 用户日志模型
 */
class MemberLog extends Model {

	public function record($user){
		/* 更新登录信息 */
		$data = array(
			'uid'             => $user['uid'],
			'login'           => array('inc', '1'),
			'last_login_time' => time(),
			'last_login_ip'   => get_client_ip(1),
		);
		Member::where(array('uid'=>$user['uid']))->update($data);
	}
}