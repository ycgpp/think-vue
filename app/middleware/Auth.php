<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\middleware;

use app\model\Member;

class Auth {

	public function handle($request, \Closure $next) {
		$header = $request->header();
		$member = new Member();
		if (isset($header['access_token']) && $header['access_token']) {
			$token = authcode($header['access_token']);
			list($uid, $username, $password) = explode('|', $token);
			$user = $member->where('uid', $uid)->where('username', $username)->find();

			if ($user && $password === $user['password']) {
				$request->user = $user->visible($member->loginVisible)->toArray();
			}else{
				$request->user = array();
				return json(['code' => 203, 'msg' => '用户登录信息失效，请重登！', 'time' => time()]);
			}
		}else{
			return json(['code' => 203, 'msg' => '用户登录信息失效，请重登！', 'time' => time()]);
		}
		return $next($request);
	}
}
