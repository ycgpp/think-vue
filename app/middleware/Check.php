<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\middleware;

use app\model\Client;

class Check {

	public function handle($request, \Closure $next) {
		$header = $request->header();
		if (isset($header['authorization']) && $header['authorization']) {
			list($appid, $sign) = explode('{|}', $header['authorization']);
			$client             = Client::where('appid', $appid)->find();
			if ($sign == md5($client['appid'] . $client['appsecret'])) {
				$request->client = $client;
			} else {
				return json(['code' => 1, 'msg' => 'authorization码错误，非法操作！', 'time' => time()]);
			}
		} else {
			return json(['code' => 1, 'msg' => '非法操作！', 'time' => time()]);
		}
		return $next($request);
	}
}
