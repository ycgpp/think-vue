<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\middleware;

use Closure;
use think\Config;
use think\Request;
use think\Response;

/**
 * 跨域请求支持
 */
class Cross {
	
	protected $cookieDomain;
	protected $header = [
		'Access-Control-Allow-Credentials' => 'true',
		'Access-Control-Allow-Methods'     => 'GET, POST, PATCH, PUT, DELETE',
		'Access-Control-Allow-Headers'     => 'authorization, access_token, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With',
	];

	public function __construct(Config $config) {
		$this->cookieDomain = $config->get('cookie.domain', '');
	}

	/**
	 * 允许跨域请求
	 * @access public
	 * @param Request $request
	 * @param Closure $next
	 * @param array   $header
	 * @return Response
	 */
	public function handle($request, Closure $next) {
		$header = $request->header();
		$header = !empty($header) ? array_merge($this->header, $header) : $this->header;
		if (!isset($header['Access-Control-Allow-Origin'])) {
			$origin = $request->header('origin');
			if ($origin && strpos($this->cookieDomain, $origin)) {
				$header['Access-Control-Allow-Origin'] = $origin;
			} else {
				$header['Access-Control-Allow-Origin'] = '*';
			}
		}

		if ($request->method(true) == 'OPTIONS') {
			return Response::create()->code(204)->header($header);
		}
		return $next($request)->header($header);
	}
}