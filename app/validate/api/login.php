<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\validate\api;

use think\Validate;

class Login extends Validate {

	protected $rule = [
		'username'   => 'require|unique:member',
		'password'   => 'require',
		'repassword' => 'require|confirm:password',
		'email'      => 'email',
		'type'       => 'require',
		'mobile'     => 'require|/^1[34578]{1}\d{9}$/',
		'code'       => 'require',
	];

	protected $message = [
		'username.require'   => '用户名不能为空',
		'username.unique'    => '用户名已存在',
		'password.require'   => '密码不能为空',
		'repassword.require' => '两次密码不一致',
		'email.email'        => '必须为邮箱',
	];

	protected $scene = [
		'register'    => ['username', 'password', 'repassword', 'type'],
		'mobile' => ['mobile', 'code'],
	];

	public function sceneIndex() {
		return $this->only(['username', 'password'])->remove('username', 'unique');
	}
}