<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\validate\api;

use think\Validate;

class Menu extends Validate {

	protected $rule = [
		'title'   => 'require',
		'path'    => 'require'
	];

	protected $message = [
		'title.require' => '栏目标题不能为空！'
	];

	protected $scene = [
		'add' => ['title', 'path']
	];
}