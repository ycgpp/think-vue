<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\controller\api;

use app\model\Member;
use app\model\Menu;
use app\model\Route;

/**
 * @title 登录注册
 * @description 登录注册
 * @group 用户
 * @header authorization:接口授权
 */
class User extends Api {

	protected $middleware = ['Api', 'Auth'];

	/**
	 * @title 用户列表
	 */
	public function lists(Member $member){
		$param = $this->request->param();
		$res = $member->paginate($param['pageSize']);

		$this->data['data'] = $res->toArray();
		$this->data['code'] = 0;
		return $this->data;
	}

	/**
	 * @title 用户详情
	 * @description 获取用户详细信息
	 */
	public function detail(Member $member){
		$uid = $this->request->param('uid', '');
		if ($uid) {
			$info = $member->where('uid', $uid)->find();
			$this->data['data'] = $info->toArray();
			$this->data['code'] = 0;
		}else{
			$this->data['msg'] = "非法操作！";
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 用户添加
	 * @description 添加用户数据
	 */
	public function add(Member $member){
		$data = $this->request->param();
		$result = $member->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '添加成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 用户编辑
	 * @description 修改用户资料信息
	 */
	public function edit(Member $member){
		$data = $this->request->param();
		$result = $member->where('uid', $data['uid'])->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '修改成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 用户删除
	 * @description 根据ID删除用户信息
	 */
	public function delete(Member $member){
		$this->data['code'] = 0;
		return $this->data;
	}

	/**
	 * @title 用户信息
	 * @description 获取用户授权的详细信息
	 */
	public function info(){
		$this->data['code'] = 0;
		$this->data['data'] = $this->request->user;
		$this->data['data']['accessInterfaces'] = array();
		$this->data['data']['userRoles'] = array();
		$this->data['data']['accessMenus'] = (new Menu())->getAccessMenus();
		$this->data['data']['accessRoutes'] = (new Route())->getAccessRoutes();
		$this->data['data']['userPermissions'] = array();
		return $this->data;
	}
}