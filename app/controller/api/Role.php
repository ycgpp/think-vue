<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\controller\api;

use app\model\Role as RoleModel;

/**
 * @title 角色管理
 * @description 角色管理
 * @group 角色管理
 * @header authorization:接口授权
 */
class Role extends Api {

	protected $middleware = ['Api', 'Auth'];

	/**
	 * @title 接口列表
	 */
	public function lists(RoleModel $role){
		$param = $this->request->param();
		$res = $role->paginate($param['pageSize']);

		$this->data['data'] = $res->toArray();
		$this->data['code'] = 0;
		return $this->data;
	}

	/**
	 * @title 添加角色
	 * @description 添加角色接口
	 */
	public function add(RoleModel $role){
		$data = $this->request->param();
		$result = $role->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '添加成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 修改角色
	 * @description 根据角色ID修改角色信息
	 */
	public function edit(RoleModel $role){
		$data = $this->request->param();
		$result = $role->where('id', $data['id'])->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '修改成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 角色删除
	 * @description 根据角色ID删除角色
	 */
	public function delete(RoleModel $role){
		$param = $this->request->param();
		if (isset($param['id']) && $param['id']) {
			if (is_array($param['id'])) {
				$map[] = array('id', 'in', $param['id']);
			}else{
				$map['id'] = $param['id'];
			}
			$result = $role->where($map)->delete();
			if (false !== $result) {
				$this->data['code'] = 0;
				$this->data['msg'] = "删除成功！";
			}else{
				$this->data['code'] = 1;
				$this->data['msg'] = "删除失败！";
			}
		}else{
			$this->data['code'] = 1;
			$this->data['msg'] = "非法操作！";
		}
		return $this->data;
	}

	/**
	 * @title 角色详情
	 * @description 根据角色ID获取角色详细信息
	 */
	public function detail(RoleModel $role){
		$id = $this->request->param('id', '');
		if (!$id) {
			$this->data['code'] = 1;
			$this->data['msg'] = '非法操作！';
		}else{
			$info = $role->where('id', $id)->find();
			$this->data['code'] = 0;
			$this->data['data'] = $info->toArray();
		}
		return $this->data;
	}

	/**
	 * @title 角色权限
	 * @description 根据角色ID获取角色权限
	 */
	public function getpermissions(){
		return $this->data;
	}
}