<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\controller\api;

use app\BaseController;

class Api extends BaseController {

	protected $data = ['code' => 1, 'data' => '', 'msg' => '', 'time' => ''];

	protected function initialize() {
		$this->data['time'] = time();
	}
}
