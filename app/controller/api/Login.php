<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\controller\api;

use app\model\Code;
use app\model\Member;
use app\model\MemberSocialite;

/**
 * @title 登录注册
 * @description 登录注册
 * @group 用户
 * @header authorization:接口授权
 */
class Login extends Api {

	protected $middleware = ['Api'];
	protected $model;

	protected function initialize() {
		parent::initialize();
		$this->model = new Member();
	}

	/**
	 * @title 用户登录
	 * @description 用户登录
	 * @author molong
	 * @url /login
	 * @method POST
	 *
	 * @header authorization:接口授权
	 *
	 * @param username:用户名 password:密码 type:int
	 *
	 * @return code:返回状态
	 * @return msg:返回消息
	 * @return time:接口服务器时间
	 * @return data:返回数据@
	 * @data uid:用户UID username:用户名 nickname:昵称 access_token:授权码
	 */
	public function index($username = '', $password = '') {
		$user = $this->model->Login($username, $password);
		if (false !== $user) {
			$this->data['code'] = 0;
			$this->data['data'] = $user;
			$this->data['msg']  = '登录成功！';
		} else {
			$this->data['code'] = 1;
			$this->data['msg']  = $this->model->error;
		}

		return json($this->data);
	}

	/**
	 * @title 用户注册
	 * @description 用户注册
	 * @author molong
	 * @url /register
	 * @method POST
	 *
	 * @header authorization:接口授权
	 *
	 * @param username:用户名 password:密码 type:int
	 *
	 * @return code:返回状态
	 * @return msg:返回消息
	 * @return time:接口服务器时间
	 * @return data:返回数据@
	 * @data uid:用户UID username:用户名 nickname:昵称 access_token:授权码
	 */
	public function register($username = '', $password = '') {
		$param = $this->request->param();

		$user = $this->model->register($param);
		if (false !== $user) {
			$this->data['code'] = 0;
			$this->data['data'] = $user;
			$this->data['msg']  = '注册成功！';
		} else {
			$this->data['code'] = 1;
			$this->data['msg']  = $this->model->error;
		}

		return json($this->data);
	}

	/**
	 * @title 手机验证码登录
	 */
	public function mobile($mobile, $code) {
		//检测验证码
		$check_code = new Code();
		if ($check_code->check($mobile, $code)) {
			$user = $this->model->where('mobile', $mobile)->find();
			if (isset($user['uid']) && $user['uid'] && $user['status']) {
				$this->data['code'] = 0;
				$this->data['data'] = $user->append(array('access_token'))->visible($this->model->loginVisible)->toArray(); //登录成功，返回用户信息
			} else {
				$data = array(
					'username' => $mobile,
					'nickname' => $mobile,
					'password' => rand_string(8),
				);
				$user = $this->model->register($data);
				if (false !== $user) {
					$this->data['code'] = 0;
					$this->data['data'] = $user;
					$this->data['msg']  = '登录成功！';
				} else {
					$this->data['code'] = 1;
					$this->data['msg']  = $this->model->error;
				}
			}
		} else {
			$this->data['code'] = 1;
			$this->data['msg']  = $check_code->error;
		}
		return json($this->data);
	}

	/**
	 * @title 第三方登录
	 */
	public function open($open_info, $type = 'wechat') {
		$socialite = new MemberSocialite();
		$open_info = $socialite->where('openid', $open_info['openid'])->where('type', $open_type)->find();
		if ($open_info) {
			$socialite->where('id', $open_info['id'])->update($open_info);
			if ($open_info['uid'] > 0) {
				//绑定了用户则自动登录
				$user = $this->model->where('uid', $open_info['uid'])->find();
				if (isset($user['uid']) && $user['uid'] && $user['status']) {
					$this->data['code'] = 0;
					$this->data['data'] = $user->append(array('access_token'))->visible($this->model->loginVisible)->toArray(); //登录成功，返回用户信息
				} else {
					$this->data['code'] = 1;
					$this->data['msg']  = "用户不存在或被禁用";
				}
			} else {
				$this->data['msg']  = "未绑定，请先绑定注册";
				$this->data['code'] = 1;
			}
		} else {
			$this->data['msg']  = "未注册，请先注册";
			$this->data['code'] = 1;
		}

		return json($this->data);
	}
}