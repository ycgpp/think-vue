<?php
// +----------------------------------------------------------------------
// | SentCMS [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.tensent.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: molong <molong@tensent.cn> <http://www.tensent.cn>
// +----------------------------------------------------------------------

namespace app\controller\api;

use app\model\Interfaces as InterfaceModel;

/**
 * @title 接口管理
 * @description 接口管理
 * @group 用户
 * @header authorization:接口授权
 */
class Interfaces extends Api {

	protected $middleware = ['Api', 'Auth'];

	/**
	 * @title 接口列表
	 */
	public function lists(InterfaceModel $interface){
		$list = $interface->getList($this->request->param());

		$this->data['data'] = $list->toArray();
		$this->data['code'] = 0;
		return $this->data;
	}

	/**
	 * @title 接口更新
	 */
	public function refresh(InterfaceModel $interface){
		$list = $interface->refreshData();

		$this->data['code'] = 0;
		return $this->data;
	}

	/**
	 * @title 添加接口
	 */
	public function add(InterfaceModel $interface){
		$data = $this->request->param();
		$result = $interface->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '添加成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 添加接口
	 */
	public function edit(InterfaceModel $interface){
		$data = $this->request->param();
		$result = $interface->where('id', $data['id'])->save($data);
		if (false !== $result) {
			$this->data['code'] = 0;
			$this->data['msg'] = '修改成功！';
		}else{
			$this->data['code'] = 1;
		}
		return $this->data;
	}

	/**
	 * @title 接口删除
	 */
	public function del(InterfaceModel $interface){
		$param = $this->request->param();
		if (isset($param['id']) && $param['id']) {
			if (is_array($param['id'])) {
				$map[] = array('id', 'in', $param['id']);
			}else{
				$map['id'] = $param['id'];
			}
			$result = $interface->where($map)->delete();
			if (false !== $result) {
				$this->data['code'] = 0;
				$this->data['msg'] = "删除成功！";
			}else{
				$this->data['code'] = 1;
				$this->data['msg'] = "删除失败！";
			}
		}else{
			$this->data['code'] = 1;
			$this->data['msg'] = "非法操作！";
		}
		return $this->data;
	}

	/**
	 * @title 接口批量删除
	 */
	public function batchdel(InterfaceModel $interface){
		$param = $this->request->param();
		if (isset($param['ids']) && $param['ids']) {
			$map[] = array('id', 'in', json_decode($param['ids']));
			$result = $interface->where($map)->delete();
			if (false !== $result) {
				$this->data['code'] = 0;
				$this->data['msg'] = "删除成功！";
			}else{
				$this->data['code'] = 1;
				$this->data['msg'] = "删除失败！";
			}
		}else{
			$this->data['code'] = 1;
			$this->data['msg'] = "非法操作！";
		}
		return $this->data;
	}

	/**
	 * @title 接口详情
	 * @description 获取接口详细信息
	 */
	public function detail(InterfaceModel $interface){
		$id = $this->request->param('id', '');
		if (!$id) {
			$this->data['code'] = 1;
			$this->data['msg'] = '非法操作！';
		}else{
			$info = $interface->where('id', $id)->find();
			$this->data['code'] = 0;
			$this->data['data'] = $info->toArray();
		}
		return $this->data;
	}
}